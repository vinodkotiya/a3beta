package com.dss1.CPEN533.A3.CPEN533_2022_A3v1;


import java.nio.ByteBuffer;
import java.util.Arrays;
	public class Utilities {
	    //Source Assignmnet Document
		 public static int test(int a) {
			 return a * 2;
		 }
		/**
	     * Int to little-endian bytes: writes x to buf[offset..]
	     */
	    public static void int2leb(int x, byte[] buf, int offset) {
	        buf[offset]=(byte)(x & 0x000000FF);
	        buf[offset+1]=(byte)((x>>8) & 0x000000FF);
	        buf[offset+2]=(byte)((x>>16) & 0x000000FF);
	        buf[offset+3]=(byte)((x>>24) & 0x000000FF);
	    }

	    /**
	     * Interprets the value of x as an unsigned byte, and returns 
	     * it as integer.  For example, ubyte2int(0xFF)==255, not -1.
	     */
	    public static int ubyte2int(byte x) {
	        return ((int)x) & 0x000000FF;
	    }
	    
	    //little endian
	    public static byte[] LE_convert2Bytes(int src) {
		//an int is equivalent to 32 bits, 4 bytes
		byte tgt[] = new byte[4];
		int mask = 0377; /* 0377 in octal*/
		
		/* this works
		tgt[3] = (byte)(src >>> 24);
		tgt[2] = (byte)((src << 8) >>> 24);
		tgt[1] = (byte)((src << 16) >>> 24);
		tgt[0] = (byte)((src << 24) >>> 24);
		*/

		tgt[3] = (byte)(src >>> 24);
		tgt[2] = (byte)((src >> 16) & 0xff);
		tgt[1] = (byte)((src >> 8)  & 0xff);
		tgt[0] = (byte)(src & 0xff);

		return tgt;
	    }
	    
	    public static String byteArrayToHexString(byte[] bytes) {
	        StringBuffer buf=new StringBuffer();
	        String       str;
	        int val;

	        for (int i=0; i<bytes.length; i++) {
	            val = ubyte2int(bytes[i]);
	            str = Integer.toHexString(val);
	            while ( str.length() < 2 )
	                str = "0" + str;
	            buf.append( str );
	        }
	        return buf.toString().toUpperCase();
	    }
	    
	    static byte[] trim(byte[] bytes)
	    {
	        int i = bytes.length - 1;
	        while (i >= 0 && bytes[i] == 0)
	        {
	            --i;
	        }

	        return Arrays.copyOf(bytes, i + 1);
	    }
	    
	    // Source: https://beginnersbook.com/2019/04/java-hexadecimal-to-decimal-conversion/
	    public static int hexToDecimal(String hexnum){  
	    	String hstring = "0123456789ABCDEF";  
	    	hexnum = hexnum.toUpperCase();  
	    	int num = 0;  
	    	for (int i = 0; i < hexnum.length(); i++)  
	    	{  
	    		char ch = hexnum.charAt(i);  
	    		int n = hstring.indexOf(ch);  
	    		num = 16*num + n;  
	    	}  
	    	return num;  
	       }
	    
	    
	}

