package com.dss1.CPEN533.A3.CPEN533_2022_A3v1;


import java.util.WeakHashMap;
import java.util.Map;

import com.dss1.CPEN533.A3.CPEN533_2022_A3v1.Server.Command;
import com.google.protobuf.ByteString;

import ca.NetSysLab.ProtocolBuffers.KeyValueRequest.KVRequest;

public class Node implements Runnable {
	static long myTotalMemoryBefore = Runtime.getRuntime().totalMemory();
	static long myMemoryUsage = 0;
    //Fields
    private WeakHashMap<ByteString, WeakHashMap<Integer,ByteString>> map = new WeakHashMap<ByteString, WeakHashMap<Integer,ByteString>>();

    //Empty constructor
    public Node() {

    }
    
    
    @SuppressWarnings("unlikely-arg-type")
	public boolean storeValue(KVRequest requestData) {
    	WeakHashMap<Integer,ByteString> key = null;
    	
    	
    	    	
    // Let's check memory usage
    	boolean ret = MemoryManager(requestData.getKey().size(),requestData.getValue().size(), 3); // send opcode 1 = Add, 2 = free, 3 = check memory
    	if (!ret)
    	{
    		//System.out.println("Memory usage exceeded. Here logic to return error code to client needs to be implemented");
    		return false;
    	}
    	
    	if(map.containsKey(requestData.getKey()))
    	{
    		key = map.get(requestData.getKey());
    		key.put(requestData.getVersion(), requestData.getValue());
    		//System.out.println(String.format("Version added is : %d and value is %s. to existing map", requestData.getVersion(), requestData.getValue()));
    	}
    	else
    	{
    		key = new WeakHashMap<Integer,ByteString>();
    		key.put(requestData.getVersion(), requestData.getValue());
    		//System.out.println(String.format("Version added is : %d and value is %s to new map", requestData.getVersion(), requestData.getValue()));
    		map.put(requestData.getKey(), key);
    	}
    	 // Let's add memory usage
    	boolean ret1 = MemoryManager(requestData.getKey().size(),requestData.getValue().size(), 1); // send opcode 1 = Add, 2 = free, 3 = check memory, 4 = clean memory
    
    	return true;
    	
    }
    
    public boolean MemoryManager(long keyLength, long valueLength, int opcode) {
    	//opcode 1 = Add, 2 = free, 3 = check memory 4 = clean memory
    	//############################################################
    	//System.out.println("keyLength " + keyLength);
    	//System.out.println("valueLength " + valueLength);
    	//System.out.println("myMemoryUsage " + myMemoryUsage);
    	   	
    	//long myTotalMemoryAfter = Runtime.getRuntime().totalMemory();
    	//long myHashMapMemory =   myTotalMemoryBefore - myTotalMemoryAfter;
    	//System.out.println("myHashMapMemory " + myHashMapMemory);
    	//System.out.println("myTotalMemoryAfter " + myTotalMemoryAfter);
    
    	
    	long myMemoryLimit = 32 * 1024 * 1024 ;    //48MB
    	long MemoryIfAdded = myMemoryUsage + keyLength + valueLength;
    	if (opcode == 3)  // check memory usage
    	{
    		if (MemoryIfAdded >  myMemoryLimit)
    	    	return false;
    	    else
    	    	return true;
    	}
    	else if (opcode == 4) { // Clean memory
     		myMemoryUsage = 0;
    	}
    	else if (opcode == 1) { // Add memory
    		if (MemoryIfAdded >  myMemoryLimit) {
         		myMemoryUsage = MemoryIfAdded;
    		}
    	}
    			
       	else if (opcode == 2) {  // free memory
       		myMemoryUsage = myMemoryUsage -(keyLength + valueLength);
       	}
       		System.out.println("myMemoryUsage " + myMemoryUsage);
    	return true;
       
      }

    public WeakHashMap<Integer,ByteString> getValue(KVRequest requestData) {
    	
    	WeakHashMap<Integer,ByteString> retValue = null;
    	if(map.containsKey(requestData.getKey()))
    	{
    		retValue = map.get(requestData.getKey());
    	}
    	return retValue;
    }

    public boolean clear() {
    	map.clear();
    	map = new WeakHashMap<ByteString, WeakHashMap<Integer,ByteString>>(); 
    	boolean ret1 = MemoryManager(1,1,4); // send opcode 1 = Add, 2 = free, 3 = check memory 4 = clean memory
       	return map.isEmpty();
    }
    
    public boolean remove(ByteString key) {
   	 // Let's free memory usage
    	boolean ret1 = MemoryManager(key.size(),map.get(key).size(), 2); // send opcode 1 = Add, 2 = free, 3 = check memory 4 = clean memory
    	
    	map.remove(key);
    	return !map.containsKey(key);
    }
    
    public boolean contains(ByteString key) {
    	return map.containsKey(key);
    }

    //Run 
    @Override
    public void run() {
        //Puts key and value pair to hashmap
        //map.put(key, value);
    }
}