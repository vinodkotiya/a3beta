package com.dss1.CPEN533.A3.CPEN533_2022_A3v1;

import java.io.IOException;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import java.io.OutputStream;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import java.util.Arrays;
import java.util.Collections;
import java.util.WeakHashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.zip.CRC32;

import com.google.protobuf.ByteString;
//import com.dss1.CPEN533.A3.CPEN533_2022_A3v1.Utilities;

import ca.NetSysLab.ProtocolBuffers.KeyValueRequest.KVRequest;
import ca.NetSysLab.ProtocolBuffers.KeyValueResponse.KVResponse;
import ca.NetSysLab.ProtocolBuffers.Message.Msg;

public class Server {

	static Node node1 = new Node();
//	static HashMap<ByteString, Msg> cache = new HashMap<ByteString, Msg>();
	static Cache<ByteString, Msg> cache = CacheBuilder.newBuilder()
            .maximumSize(1200)
            .expireAfterWrite(5, TimeUnit.SECONDS)
            .build();

	public static void main(String[] args) throws Exception {

		DatagramSocket serverSocket = new DatagramSocket(8000);
		DatagramPacket incomingDataPacket = new DatagramPacket(new byte[16000], 16000);
		System.out.println("The server is running");

		while (true) {
			serverSocket.receive(incomingDataPacket);

			byte[] dataByeArray = Arrays.copyOfRange(incomingDataPacket.getData(), 0, incomingDataPacket.getLength());
			Msg clientMessage = Msg.parseFrom(dataByeArray);
			// Long messageChecksum = clientMessage.getCheckSum();
			ByteString messageID = clientMessage.getMessageID();
			ByteString messagePayload = clientMessage.getPayload();
			// System.out.println("responseChecksum" + messageChecksum);
			// System.out.println(messageID);
			// System.out.println(messagePayload);

			long clientChecksum = clientMessage.getCheckSum();
			byte[] payloadArr = messagePayload.toByteArray();
			byte[] messageArr = messageID.toByteArray();
			byte[] responseByteArray = new byte[messageArr.length + payloadArr.length];
			System.arraycopy(messageArr, 0, responseByteArray, 0, messageArr.length);
			System.arraycopy(payloadArr, 0, responseByteArray, messageArr.length, payloadArr.length);
			CRC32 calculatedChecksum = new CRC32();
			calculatedChecksum.update(responseByteArray);


			if (clientChecksum == calculatedChecksum.getValue()) {

				Msg cachedResponse =  cache.getIfPresent(messageID);
				
				if (cachedResponse != null) {
					byte[] bytesToSend = cachedResponse.toByteArray();
					DatagramPacket response = new DatagramPacket(bytesToSend, bytesToSend.length,
							incomingDataPacket.getAddress(), incomingDataPacket.getPort());
					serverSocket.send(response);
				}
				else {
					KVRequest kvRequest = KVRequest.parseFrom(messagePayload);
					int command = kvRequest.getCommand();
					ByteString key = kvRequest.getKey();
					ByteString value = kvRequest.getValue();
					int version = kvRequest.getVersion();
					System.out.println(String.format("Command is: %s, command id is: %d, version is:%d",
							Command.valueOf(command), command, version));
					System.out.println(String.format("Key, Values lengths are: %d, %d", key.toByteArray().length,
							value.toByteArray().length));

					if (key.size() > 32) {
						KVResponse.Builder kvResponse = KVResponse.newBuilder();
						kvResponse.setErrCode(6);
						sendResponse(kvResponse.build(), messageID, serverSocket, incomingDataPacket);
					}
					if (value.size() > 10000) {
						KVResponse.Builder kvResponse = KVResponse.newBuilder();
						kvResponse.setErrCode(7);
						sendResponse(kvResponse.build(), messageID, serverSocket, incomingDataPacket);
					}

					switch (command) {
					case 1:

						if (node1.storeValue(kvRequest)) {
							put(messageID, serverSocket, incomingDataPacket);
						} else {
							KVResponse.Builder kvResponse = KVResponse.newBuilder();
							kvResponse.setErrCode(2);
							sendResponse(kvResponse.build(), messageID, serverSocket, incomingDataPacket);
						}
						break;
					case 2:
						get(messageID, kvRequest, serverSocket, incomingDataPacket, node1.getValue(kvRequest));
						break;
					case 3:
						remove(messageID, kvRequest, serverSocket, incomingDataPacket);
						break;
					case 4:
						shutdown();
						break;
					case 5:
						wipeout(messageID, serverSocket, incomingDataPacket);
						break;
					case 6:
						isAlive(messageID, serverSocket, incomingDataPacket);
						break;
					case 7:
						getPID(messageID, serverSocket, incomingDataPacket);
						break;
					case 8:
						getMembershipCount(messageID, serverSocket, incomingDataPacket);
						break;
					default:
						KVResponse.Builder kvResponse = KVResponse.newBuilder();
						kvResponse.setErrCode(5);
						sendResponse(kvResponse.build(), messageID, serverSocket, incomingDataPacket);
					}
				}
			}

		}

	}

	enum Command {
		PUT(1), // Put: This is a put operation
		Get(2), // Get: This is a get operation.
		Remove(3), // Remove: This is a remove operation.
		Shutdown(4), // Shutdown: shuts-down the node (used for testing and management). The expected
						// behaviour is that your implementation immediately calls System.exit().
		Wipeout(5), // Wipeout: deletes all keys stored in the node (used for testing)
		IsAlive(6), // IsAlive: does nothing but replies with success if the node is alive.
		GetPID(7), // GetPID: the node is expected to reply with the processID of the Java process
		GetMembershipCount(8); // the node is expected to reply with the count of the currently active members
								// based on your membership protocol. (this will be used later, for now you are
								// expected to return 1)

		private static Map<Integer, Command> map = new WeakHashMap<>();
		private int value;

		private Command(int value) {
			this.value = value;
		}

		static {
			for (Command command : Command.values()) {
				map.put(command.value, command);
			}
		}

		public static Command valueOf(int command) {
			return (Command) map.get(command);
		}

		public int getValue() {
			return value;
		}

	}

	public static void put(ByteString messageID, DatagramSocket socket, DatagramPacket client) throws IOException {
		KVResponse.Builder kvResponse = KVResponse.newBuilder();
		kvResponse.setErrCode(0);
		sendResponse(kvResponse.build(), messageID, socket, client);

	}

	public static void get(ByteString messageID, KVRequest request, DatagramSocket socket, DatagramPacket client,
			WeakHashMap<Integer, ByteString> data) throws IOException {
		KVResponse.Builder kvResponse = KVResponse.newBuilder();

		if (data == null) {
			kvResponse.setErrCode(1);
		} else {
			data.entrySet().forEach(entry -> {
				System.out.println(entry.getKey() + " " + entry.getValue());
			});
			int version = Collections.max(data.keySet());
			ByteString value = data.get(version);
			System.out.println(
					String.format("Version requested is : %d and returned is : %d", request.getVersion(), version));
			kvResponse.setValue(value);
			kvResponse.setVersion(version);
			kvResponse.setErrCode(0);
		}
		sendResponse(kvResponse.build(), messageID, socket, client);
	}

	public static void remove(ByteString messageID, KVRequest request, DatagramSocket socket, DatagramPacket client)
			throws IOException {
		KVResponse.Builder kvResponse = KVResponse.newBuilder();
		if (node1.contains(request.getKey())) {
			kvResponse.setErrCode(node1.remove(request.getKey()) ? 0 : 1);
		} else {
			kvResponse.setErrCode(1);
		}
		sendResponse(kvResponse.build(), messageID, socket, client);
	}

	public static void shutdown() {
		System.exit(0);
	}

	public static void wipeout(ByteString messageID, DatagramSocket socket, DatagramPacket client) throws IOException {
		KVResponse.Builder kvResponse = KVResponse.newBuilder();
		kvResponse.setErrCode(node1.clear() ? 0 : 1);
		sendResponse(kvResponse.build(), messageID, socket, client);
	}

	public static void isAlive(ByteString messageID, DatagramSocket socket, DatagramPacket client) throws IOException {
		KVResponse.Builder kvResponse = KVResponse.newBuilder();
		kvResponse.setErrCode(0);
		sendResponse(kvResponse.build(), messageID, socket, client);

	}

	public static void getPID(ByteString messageID, DatagramSocket socket, DatagramPacket client) throws IOException {
		KVResponse.Builder kvResponse = KVResponse.newBuilder();
		long processID = getCurrentProcessId();
		kvResponse.setPid((int)processID);  // TODO : hard coding to be removed.
		// kvResponse.setVersion(version);
		kvResponse.setErrCode(0);
		sendResponse(kvResponse.build(), messageID, socket, client);
	}

	public static void getMembershipCount(ByteString messageID, DatagramSocket socket, DatagramPacket client)
			throws IOException {
		KVResponse.Builder kvResponse = KVResponse.newBuilder();
		kvResponse.setMembershipCount(1);
		kvResponse.setErrCode(0);
		sendResponse(kvResponse.build(), messageID, socket, client);
	}

	public static void sendResponse(KVResponse kvResponse, ByteString messageID, DatagramSocket socket,
			DatagramPacket client) throws IOException {

		ByteString payload = kvResponse.toByteString();
		byte[] payloadArr = payload.toByteArray();
		byte[] messageArr = messageID.toByteArray();
		byte[] responseByteArray = new byte[messageArr.length + payloadArr.length];
		System.arraycopy(messageArr, 0, responseByteArray, 0, messageArr.length);
		System.arraycopy(payloadArr, 0, responseByteArray, messageArr.length, payloadArr.length);

		CRC32 responseChecksum = new CRC32();
		responseChecksum.update(responseByteArray);
		Msg responseMsg = Msg.newBuilder().setMessageID(messageID).setPayload(payload)
				.setCheckSum(responseChecksum.getValue()).build();
		



	    cache.put(messageID, responseMsg);

//	    assertEquals("someValue", cache.getIfPresent("someKey"));
//		cache.put(messageID, responseMsg);
		//int totalSize = cache.size() *
		/*Timer timer = new Timer();
		timer.schedule(new TimerTask() {
		  @Override
		  public void run() {
			  if (cache.containsKey(messageID))
				{
				  cache.remove(messageID);
				  
				}
		  }
		}, 0, 5000); //wait 5 secs to delete */
		
		byte[] bytesToSend = responseMsg.toByteArray();
		DatagramPacket response = new DatagramPacket(bytesToSend, bytesToSend.length, client.getAddress(),
				client.getPort());
		socket.send(response);
	}

	/*
	 * private static int getCurrrentProcessId() throws Exception {
	 * 
	 * RuntimeMXBean runtime = ManagementFactory.getRuntimeMXBean(); Field jvm =
	 * runtime.getClass().getDeclaredField("jvm"); jvm.setAccessible(true);
	 * 
	 * VMManagement management = (VMManagement) jvm.get(runtime); Method method =
	 * management.getClass().getDeclaredMethod("getProcessId");
	 * method.setAccessible(true);
	 * 
	 * return (Integer) method.invoke(management); }
	 */
	
	private static long getCurrentProcessId(){		
		RuntimeMXBean bean = ManagementFactory.getRuntimeMXBean();
		// Get name representing the running Java virtual machine.
        // It returns something like 16516@Krakatau. The value before 
        // the @ symbol is the PID.
        String jvmName = bean.getName();
		System.out.println("Name = " + jvmName);

        // Extract the PID by splitting the string returned by the
        // bean.getName() method.
        long pid = Long.parseLong(jvmName.split("@")[0]);
        return pid;
	}
}